<?php

class MegaSena
{
    public function megasena($num, $valor)
    {
        $list = [];

        foreach ($num as $i => $v) {
            if ($v < 1 || $v > 60) {
                return 0.0; // inválido
            }

            if (in_array($v, $list)) {
                return 0.0; //repetido
            }

            array_push($list, $v);
        }

        if (count($list) >= 6 && count($list) <= 15) {
            $sort = [];
            $s = 0;
            while (count($sort) < 6) {
                $s = rand(0, 60);
                if (!in_array($s, $sort)) {
                    array_push($sort, $s);
                }
            }


            $tot = 0;
            foreach ($num as $i => $v) {
                if (in_array($sort, $v)) {
                    $tot++;
                }
            }

            if ($tot == 6) {
                return $valor; // 100%
            } elseif ($tot == 5) {
                return $valor * 0.2; // 20%
            } elseif ($tot == 4) {
                return $valor * 0.05; // 5%
            }
        }
        return 0.0;
    }
}
