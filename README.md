# 🧼 clean-code

Projeto destinado para demonstração de conhecimento relacionado a cleancode.

## ❓ Sobre

Repositório estruturado de forma para entender o processo de refatoração de um código sujo. Nele, foi criado uma classe para sorteio da MegaSena, onde de principio foi construído todo processo, desde a validação até o sorteio dos números, em apenas um método.

# 🧹 Pasta: codigo-sujo

Nessa pasta está o código sujo, onde a classe possui apenas um método responsável pela validação, pelo sorteio, pelo calculo de acertos e pelo retorno do prêmio. Além de possuir apenas um método "faz tudo", a nomenclatura das variáveis e os comentários não facilitam o entendimento da classe.

# 🔧 Pasta: refatoracao


## 🔤 Sub-Pasta: 1_melhorando_nomenclaturas

O primeiro passo na refatoração para melhorar o entendimento da classe foi alterar a nomenclatura das variáveis. Antes, o nome das variáveis eram genéricas, não facilitando o entendimento da funcionalidade que elas possuem no método. Agora, com a alteração das nomenclaturas, ficou mais claro a função de cada variável.

## 📤 Sub-Pasta: 2_extracao_de_metodos

O segundo passo na refatoração foi dividir as funcionalidades em métodos diferentes. O método `calculaPremio()` antes possuía mais funcionalidades do que ele deveria fazer, que é calcular o prêmio do usuário. Porém, com métodos distintos para esssas demais funcionalidades, o método `calculaPremio()` possuí apenas o código necessário para calcular o prêmio, tornando as demais validações e calculos apenas pré-requisitos que os outros métodos são responsáveis por realizar.
Com métodos mais únicos, possuíndo apenas funcionalidades próprias, facilita, e muito, a legibilidade do código.

## ✔️ Sub-Pasta: 3_finalizando

O terceiro passo foi a adição de exceções para quando um argumento não passar pelas validações, Também alterado a forma de premiação, antes os números de acertos necessários para cada tempo ficavam fixas no meio do código, agora são declaradas em funções, possuindo uma nomenclatura clara e caso precise alterar os números de acertos necessários, precisam alterar em apenas um local.
