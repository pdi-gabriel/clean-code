<?php

class MegaSena
{
    public function calculaPremio($numerosApostados, $premioTotal)
    {
        if (!$this->isApostaValida($numerosApostados)) {
            return 0.0;
        }
        $numerosSorteados = $this->sorteiaSeisNumeros();
        $acertos = $this->calculaAcertos($numerosApostados, $numerosSorteados);

        if ($acertos == 6) {
            return $premioTotal; // Sena = 100%
        } elseif ($acertos == 5) {
            return $premioTotal * 0.2; // Sena = 20%
        } elseif ($acertos == 4) {
            return $premioTotal * 0.05; // Sena = 5%
        }
        return 0.0;
    }


    private function isApostaValida($numerosApostados)
    {
        if (count($numerosApostados) < 6 || count($numerosApostados) > 15) {
            return false;
        }
        $numerosValidos = [];
        foreach ($numerosApostados as $i => $apostado) {
            if ($apostado < 1 || $apostado > 60) {
                return false; // inválido
            }
            if (in_array($apostado, $numerosValidos)) {
                return false; // repetido
            }
            array_push($numerosValidos, $apostado);
        }
        return true;
    }


    private function sorteiaSeisNumeros()
    {
        $numerosSorteados = [];
        $numeroSorteado = 0;
        while (count($numerosSorteados) < 6) {
            $numeroSorteado = rand(0, 60);
            if (!in_array($numeroSorteado, $numerosSorteados)) {
                array_push($numerosSorteados, $numeroSorteado);
            }
        }
        return $numerosSorteados;
    }

    private function calculaAcertos($apostados, $sorteados)
    {
        $acertos = 0;
        foreach ($apostados as $i => $apostado) {
            if (in_array($sorteados, $apostado)) {
                $acertos++;
            }
        }
        return $acertos;
    }
}
