<?php

class MegaSena
{
    public function calculaPremio($numerosApostados, $premioTotal)
    {
        $numerosValidos = [];

        foreach ($numerosApostados as $i => $apostado) {
            if ($apostado < 1 || $apostado > 60) {
                return 0.0; //invalido
            }

            if (in_array($apostado, $numerosValidos)) {
                return 0.0; //repetido
            }

            array_push($numerosValidos, $apostado);
        }

        if (count($numerosValidos) >= 6 && count($numerosValidos) <= 15) {
            $numerosSorteados = [];
            $numeroSorteada = 0;
            while (count($numerosSorteados) < 6) {
                $numeroSorteada = rand(0, 60);
                if (!in_array($numeroSorteada, $numerosSorteados)) {
                    array_push($numerosSorteados, $numeroSorteada);
                }
            }


            $acertos = 0;
            foreach ($numerosApostados as $i => $apostado) {
                if (in_array($numerosSorteados, $apostado)) {
                    $acertos++;
                }
            }

            if ($acertos == 6) {
                return $premioTotal; // Sena = 100%
            } elseif ($acertos == 5) {
                return $premioTotal * 0.2; // Sena = 20%
            } elseif ($acertos == 4) {
                return $premioTotal * 0.05; // Sena = 5%
            }
        }
        return 0.0;
    }
}
